Studentnummer:
Naam:

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Nee

Waar worden Branches vaak voor gebruikt?
	# Voor feautures/bugfixes

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git branch
 -Nieuwe Branch aanmaken
	# git branch [branchnaam]
 -Van Branch wisselen
	# git branch [branchnaam] (LOCAL)
 -Branch verwijderen
        # git branch -d [branchnaam] 
        # git push origin --delete push origin
